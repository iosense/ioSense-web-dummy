FROM node:boron

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY ioSense-web-dummy /usr/src/app/
RUN npm config set registry http://registry.npmjs.org/
RUN npm install

EXPOSE 5002
CMD ["node","index.js"]
