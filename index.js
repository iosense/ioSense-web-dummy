// === === === ioSense Setting === === ===

var ioSenseSettings = {
    host: "iosense-docker-nodejs",              // host where the web server is deployed
    namespace: "trial",          // namespace
    port: 443,                     // application port
    client_id: "70a40cb8307740a0ac09895454",
    secret: "fab31c17aaccc433a5dc4033b901bb66bcaabe26c8fcba01f8dd4146ce0c29c9c9f91ea3d74622010e0ba4200e8cb3b9"
};

// client_id: "c46306a49f294530a094f37fb",
// secret: "022bec37e2be3fd5b673fe19cfde0b09023fa3f7204bacb93f5179cdef9c814ce1e99fda4fe8ffd08b06a360f7b2fff9"

var ioSenseURI = 'https://' + ioSenseSettings.host + ":" + ioSenseSettings.port;


// where to deploy the web server
var host = "localhost";
var port = 5002;


// Loading the ioSense SDK
var ioSense = require('iosense-js-sdk');

// Give a name to your application (to be used as source's name in ioSense)
var applicationName = "ioSense Hello World Web App";

// initialize the settings
ioSense.init(ioSenseSettings);

// === === === General Web Server Setting === === ===

// Setting the logs
var log4js = require('log4js');
var logger = log4js.getLogger('[ioSense-web-dummy]');
logger.level = 'INFO';

// Dependencies for the nodeJS web server
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var moment = require('moment');

// Web server settings
var app = express();
var http = require('http').Server(app);
var fs = require("fs");
var render = require('co-views')('views');
app.use(express.static(path.join(__dirname, 'public'), {etag: false, maxAge: 100}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


/**
 * === === === === === Template engine === === === === ===
 */

app.engine('ntl', function (filePath, options, callback) { // define the template engine
    fs.readFile(filePath, function (err, content) {
        if (err) return callback(err)
        // this is an extremely simple template engine
        var rendered = content.toString();
        for (var key in options) {
            rendered = rendered.replace('#' + key + '#', options[key]);
        }

        return callback(null, rendered)
    })
});
app.set('views', './views');        // specify the views directory
app.set('view engine', 'ntl');      // register the template engine


// === === === Web app API === === ===

/**
 * Root of the web application, pointing to the file /public/home.html
 */
app.get("/", function (req, res) {
    res.render('home', {
        ioSense_login_uri: 'https://iosense.tudelft.nl/' + ioSenseSettings.namespace + '/oauth/authorize?' +
        'client_id=' + ioSenseSettings.client_id + '&redirect_uri=http://' + host + ':' + port + '/'
    });
});

/**
 * Check the validity of the token.
 */
app.get("/login", function (req, res) {
    ioSense.connectWithToken(req.get('Authorization'), function (error, data) {
        if (error) {
            logger.error(error);
        } else {
            res.status(200).json(data);
        }
    });
});

/**
 * This start listening on the given port, making your webapp available.
 */
http.listen(port, function () {
    logger.info('ioSense Hello World WebApp is listening on port : ' + port);
});