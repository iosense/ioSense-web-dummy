
// Access token received when logging in or stored in the local storage
var authorizationToken = null;

var source;

/**
 * Because there are several cases to get the access token (freshly from log in
 * or from local storage), we want to make sure the token is valid.
 * @param token
 */
function login(token) {
    authorizationToken = token;
    $.ajax({
        type: "GET",
        url: "/login",
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", authorizationToken);
        },
        timeout: 2000,
        error: function (error) {
            console.log(error);
            localStorage.removeItem("token");
            authorizationToken = null;
        },
        success: function (data) {
            if (data.success!==undefined && data.success) {
                // The log in was successful, we store the token in the
                // local storage for future request, then we update the screen with loggedIn()
                localStorage.setItem("token", token);
                loggedIn();
            }
        }
    });
}

function logOut() {
    localStorage.removeItem("token");
    authorizationToken = null;
    document.location.href = "/";
}

/**
 * Called when the log in succeed to show the page
 * and load the charts.
 */
function loggedIn() {
    // show the content of the page
    $('body').css("display", "block");
    createSource(function () {
        setInterval(generateData, 1000);
    })
}

function generateData() {
    var datapoints = [];
    var ts = new Date().getTime();
    source.streams.forEach(function(stream) {
        datapoints.push(stream.id, ts, Math.random());
    });
    pushData({datapoints:datapoints});
}

function createSource(callback) {

    var streams = [];
    for (var i=0;i<20;i++) {
        streams.push({
            name: "Dummy " + i,
            description: "Dummy data " + i,
            type: "DUMMY",
            monitored: true
        })
    }

    var newSource = {
        name: "My dummy source",
        description: "My dummy source",
        type: "Dummy",
        streams: streams
    };


    $.ajax({
        type: "POST",
        url: "https://iosense.tudelft.nl/trial/sources",
        data: newSource,
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", authorizationToken);
        },
        timeout: 2000,
        error: function (error) {
            $('#error').html(error);
        },
        success: function (data) {
            source = data.source;
            $('#source').html(source);
            callback();
        }
    });
}

function pushData(data) {
    $.ajax({
        type: "POST",
        url: "https://iosense.tudelft.nl/trial/datapoints",
        data: data,
        jsonp: "callback",
        dataType: "jsonp",
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", authorizationToken);
        },
        timeout: 2000,
        error: function (error) {
            $('#error').html($('#error').html() + "<br>" + error);
        },
        success: function (data) {
            $('#pushedData').html(data);
        }
    });
}